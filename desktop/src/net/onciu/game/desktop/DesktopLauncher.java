package net.onciu.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import net.onciu.game.AriGame;

public class DesktopLauncher {

    private static boolean rebuildAtlas = true;
    private static boolean drawDebugOutline = false;

    public static void main(String[] arg) {

        if (rebuildAtlas) {
            TexturePacker.Settings settings = new TexturePacker.Settings();
            settings.maxWidth = 1024;
            settings.maxHeight = 1024;
            settings.duplicatePadding = true;
            settings.debug = drawDebugOutline;
            TexturePacker.process(settings, "images", ".", "ari");
        }

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        new LwjglApplication(new AriGame(), config);
    }
}
