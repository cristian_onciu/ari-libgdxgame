package net.onciu.game;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.math.MathUtils;

import com.badlogic.gdx.utils.Array;
import net.onciu.game.util.Assets;
import net.onciu.game.util.CameraHelper;
import net.onciu.game.util.Constants;

import java.util.logging.Logger;

/**
 * Created by Cristian Nicolae Onciu on 03/05/15.
 */
public class WorldController extends InputAdapter {
    private static final String TAG = WorldController.class.getCanonicalName();

    public Level level;
    public int lives;
    public int score;

    //public Sprite[] testSprites;
    //public int selectedSprite;

    public CameraHelper cameraHelper;

    public WorldController() {
        init();
    }

    private void initLevel () {
        score = 0;
        level = new Level(Constants.LEVEL_01);
    }

    private void init() {
        Gdx.input.setInputProcessor(this);
        cameraHelper = new CameraHelper();
        //initTestObjects();
        //initTestAssetObjects();
        lives = Constants.LIVES_START;
        initLevel();
    }

    /*
    private void initTestAssetObjects() {
        // Create new array for 5 sprites
        testSprites = new Sprite[5];
        // Create a list of texture regions
        Array<TextureRegion> regions = new Array<TextureRegion>();
        regions.add(Assets.INSTANCE.bunny.head);
        regions.add(Assets.INSTANCE.feather.feather);
        regions.add(Assets.INSTANCE.goldCoin.goldCoin);
        // Create new sprites using a random texture region
        for (int i = 0; i < testSprites.length; i++) {
            Sprite spr = new Sprite(regions.random());
            // Define sprite size to be 1m x 1m in game world
            spr.setSize(1, 1);
            // Set origin to sprite's center
            spr.setOrigin(spr.getWidth() / 2.0f,
                    spr.getHeight() / 2.0f);
            // Calculate random position for sprite
            float randomX = MathUtils.random(-2.0f, 2.0f);
            float randomY = MathUtils.random(-2.0f, 2.0f);
            spr.setPosition(randomX, randomY);
            // Put new sprite into array
            testSprites[i] = spr;
        }
        // Set first sprite as selected one
        selectedSprite = 0;
    }

    private void initTestObjects() {
        // Create new array for 5 sprites
        testSprites = new Sprite[5];
        // Create empty POT-sized Pixmap with 8 bit RGBA pixel data
        int width = 32;
        int height = 32;
        Pixmap pixmap = createProceduralPixmap(width, height);
        // Create a new texture from pixmap data
        Texture texture = new Texture(pixmap);
        // Create new sprites using the just created texture
        for (int i = 0; i < testSprites.length; i++) {
            Sprite spr = new Sprite(texture);
            // Define sprite size to be 1m x 1m in game world
            spr.setSize(1, 1);
            // Set origin to sprite's center
            spr.setOrigin(spr.getWidth() / 2.0f, spr.getHeight() / 2.0f);
            // Calculate random position for sprite
            float randomX = MathUtils.random(-2.0f, 2.0f);
            float randomY = MathUtils.random(-2.0f, 2.0f);
            spr.setPosition(randomX, randomY);
            // Put new sprite into array
            testSprites[i] = spr;
        }
        // Set first sprite as selected one
        selectedSprite = 0;
    }
    */

    private Pixmap createProceduralPixmap(int width, int height) {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        // Fill square with red color at 50% opacity
        pixmap.setColor(1, 0, 0, 0.5f);
        pixmap.fill();
        // Draw a black-colored X shape on square
        pixmap.setColor(0, 0, 0, 1);
        pixmap.drawLine(0, 0, width, height);
        pixmap.drawLine(width, 0, 0, height);
        // Draw a black-colored border around square
        pixmap.setColor(0, 0, 0, 1);
        pixmap.drawRectangle(0, 0, width, height);
        return pixmap;
    }

    /*
    private void updateTestObjects(float deltaTime) {
        // Get current rotation from selected sprite
        float rotation = testSprites[selectedSprite].getRotation();
        // Rotate sprite by 90 degrees per second
        rotation += 90 * deltaTime;
        // Wrap around at 360 degrees
        rotation %= 360;
        // Set new rotation value to selected sprite
        testSprites[selectedSprite].setRotation(rotation);
    }
    */

    private void handleDebugInput(float deltaTime) {
        // Selected Sprite Controls
        float sprMoveSpeed = 2 * deltaTime;

        // Camera Controls (move)
        float camMoveSpeed = 2 * deltaTime;
        float camMoveSpeedAccelerationFactor = 2;

        // Camera Controls (zoom)
        float camZoomSpeed = 1 * deltaTime;
        float camZoomSpeedAccelerationFactor = 5;
        if (Gdx.app.getType() == ApplicationType.Android) {
            Gdx.app.debug(TAG, "Android here");
            //Check for accelerometer
            boolean available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer);
            if (available) {
                float accelX = Gdx.input.getAccelerometerX();

                float accelY = Gdx.input.getAccelerometerY();

                float accelZ = Gdx.input.getAccelerometerZ();

                if (Math.abs(accelX) > 0.3f) {
                    Gdx.app.debug(TAG, "accelX: " + accelX);
                    /*
                    if (accelX < 0) moveSelectedSprite(0, sprMoveSpeed);
                    else if (accelX > 0) moveSelectedSprite(0, -sprMoveSpeed);
                    */
                }


                if (Math.abs(accelY) > 0.3f) {
                    Gdx.app.debug(TAG, "accelY: " + accelY);
                    /*
                    if (accelY < 0) moveSelectedSprite(-sprMoveSpeed, 0);
                    else if (accelY > 0) moveSelectedSprite(sprMoveSpeed, 0);
                    */
                }

                //Gdx.app.debug(TAG, "accelZ: " + accelZ);
                /*
                if (accelZ > 10f) cameraHelper.addZoom(
                        camZoomSpeed);
                else if (accelZ < 9f) cameraHelper.addZoom(
                        -camZoomSpeed);
                        */

            } else Gdx.app.debug(TAG, "No accelerometer detected");
        } else {
            // Selected Sprite Controls
            /*
            if (Gdx.input.isKeyPressed(Keys.A)) moveSelectedSprite(
                    -sprMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Keys.D))
                moveSelectedSprite(sprMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Keys.W)) moveSelectedSprite(0,
                    sprMoveSpeed);
            if (Gdx.input.isKeyPressed(Keys.S)) moveSelectedSprite(0,
                    -sprMoveSpeed);
                    */

            // Camera Controls (move)
            if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) camMoveSpeed *=
                    camMoveSpeedAccelerationFactor;
            if (Gdx.input.isKeyPressed(Keys.LEFT)) moveCamera(-camMoveSpeed,
                    0);
            if (Gdx.input.isKeyPressed(Keys.RIGHT)) moveCamera(camMoveSpeed,
                    0);
            if (Gdx.input.isKeyPressed(Keys.UP)) moveCamera(0, camMoveSpeed);
            if (Gdx.input.isKeyPressed(Keys.DOWN)) moveCamera(0,
                    -camMoveSpeed);
            if (Gdx.input.isKeyPressed(Keys.BACKSPACE))
                cameraHelper.setPosition(0, 0);

            // Camera Controls (zoom)
            if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) camZoomSpeed *=
                    camZoomSpeedAccelerationFactor;
            if (Gdx.input.isKeyPressed(Keys.COMMA))
                cameraHelper.addZoom(camZoomSpeed);
            if (Gdx.input.isKeyPressed(Keys.PERIOD)) cameraHelper.addZoom(
                    -camZoomSpeed);
            if (Gdx.input.isKeyPressed(Keys.SLASH)) cameraHelper.setZoom(1);
        }
    }

    private void moveCamera(float x, float y) {
        x += cameraHelper.getPosition().x;
        y += cameraHelper.getPosition().y;
        cameraHelper.setPosition(x, y);
    }

    /*
    private void moveSelectedSprite(float x, float y) {
        testSprites[selectedSprite].translate(x, y);
    }
    */

    public void update(float deltaTime) {
        handleDebugInput(deltaTime);
        //updateTestObjects(deltaTime);
        cameraHelper.update(deltaTime);
    }

    @Override
    public boolean keyUp(int keycode) {
        // Reset game world
        if (keycode == Keys.R) {
            init();
            Gdx.app.debug(TAG, "Game world resetted");
        }
        // Select next sprite
        /*
        else if (keycode == Keys.SPACE) {
            selectedSprite = (selectedSprite + 1) % testSprites.length;
            // Update camera's target to follow the currently
            // selected sprite
            if (cameraHelper.hasTarget()) {
                cameraHelper.setTarget(testSprites[selectedSprite]);
            }
            Gdx.app.debug(TAG, "Sprite #" + selectedSprite + " selected");
        }
        */
        // Toggle camera follow
        /*
        else if (keycode == Keys.ENTER) {
            cameraHelper.setTarget(cameraHelper.hasTarget() ? null :
                    testSprites[selectedSprite]);
            Gdx.app.debug(TAG, "Camera follow enabled: " +
                    cameraHelper.hasTarget());
        }
        */
        return false;
    }
}
